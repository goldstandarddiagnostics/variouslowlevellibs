namespace System.Device.Motion.Pump;

interface

type
  /// <summary>This interface contains the basic pump commands</summary>
  IPump = public interface
    /// <summary>StartDispensing dispenses a certain volume with a selected speed . It is a non blocking operation and must be used in combination with WaitForPumpActionToComplete.</summary>
    /// <param name="aVolume">the volume to dispense. (in 無)</param>
    /// <param name="aSpeed">The speed of the pump. (in  無 / second)</param>
    method StartDispensing(aVolume, aSpeed: Double);
    /// <summary>StartAspirating aspirates a certain volume with a selected speed . It is a non blocking operation and must be used in combination with WaitForPumpActionToComplete.</summary>
    /// <param name="aVolume">the volume to aspirate. (in 無)</param>
    /// <param name="aSpeed">The speed of the pump. (in  無 / second)</param>
    method StartAspirating(aVolume, aSpeed: Double);
    /// <summary>WaitForPumpActionToComplete waits until the pump action is complete.</summary>
    method WaitForPumpActionToComplete;
  end;

  /// <summary>This interface contains the basic Syringe commands</summary>
  ISyringe = public interface(IPump)
    /// <summary>StartDispenseAll dispenses all fluid in the syringe the at the selected speed . It is a non blocking operation and must be used in combination with WaitForPumpActionToComplete.</summary>
    /// <param name="aSpeed">The speed of the pump. (in  無 / second)</param>
    method StartDispenseAll(aSpeed: System.Double);
    /// <summary>AspiratedVolume returns the volume currently in the syringe. (in 無)</summary>
    property AspiratedVolume: Double read;
    /// <summary>MaxPossibleVolume returns the maximum volume the syringe can pickup. (in 無)</summary>
    property MaxPossibleVolume: Double read;
  end;

implementation

end.