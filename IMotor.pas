﻿namespace System.Device.Motion;

interface

uses
  Prism.StandardAspects;

type
  /// <summary>This interface contains the basic motor commands</summary>
  IMotor = public interface
    /// <summary>MoveToPosition moves the motor to a specified position with a certain acceleration, speed and deceleration. It is a blocking operation and will return when it reached it's destination position.</summary>
    /// <param name="aPosition">The target position of the motor. (in motor steps)</param>
    /// <param name="aAcceleration">The acceleration of the motor. (in motor steps / second²)</param>
    /// <param name="aSpeed">The speed of the motor. (in motor steps / second)</param>
    /// <param name="aDeceleration">The deceleration of the motor. (in motor steps / second²)</param>
    method MoveToPosition(aPosition: Int32; aAcceleration: Int32; aSpeed: Word; aDeceleration: Int32);
    /// <summary>MoveToPosition moves the motor to a specified position with a certain acceleration, speed and deceleration. It is a non blocking operation and will return immediatly (use this in combination with WaitForMoveToPosition).</summary>
    /// <param name="aPosition">The target position of the motor. (in motor steps)</param>
    /// <param name="aAcceleration">The acceleration of the motor. (in motor steps / second²)</param>
    /// <param name="aSpeed">The speed of the motor. (in motor steps / second)</param>
    /// <param name="aDeceleration">The deceleration of the motor. (in motor steps / second²)</param>
    method StartMoveToPosition(aPosition: Int32; aAcceleration: Int32; aSpeed: Word; aDeceleration: Int32);
    /// <summary>WaitForMoveToPosition waits until StartMoveToPosition finishes.</summary>
    method WaitForMoveToPosition();
    /// <summary>MoveClockWise moves the motor clockwise with a certain acceleration and speed</summary>
    /// <param name="aAcceleration">The acceleration of the motor. (in motor steps / second²)</param>
    /// <param name="aSpeed">The speed of the motor. (in motor steps / second)</param>
    method MoveClockWise(aAcceleration: Int32; aSpeed: UInt16);
    /// <summary>MoveCounterClockWise moves the motor counterclockwise with a certain acceleration and speed</summary>
    /// <param name="aAcceleration">The acceleration of the motor. (in motor steps / second²)</param>
    /// <param name="aSpeed">The speed of the motor. (in motor steps / second)</param>
    method MoveCounterClockWise(aAcceleration: Int32; aSpeed: Word);
    /// <summary>Stops the motor with a certain deceleration</summary>
    /// <param name="aDeceleration">The deceleration of the motor. (in motor steps / second²)</param>
    method Stop(aDeceleration: Int32);
    /// <summary>Gets the number of steps needed to do one full revolution</summary>
    /// <value>Number of steps per revolution</value>
    /// <returns>Number of steps per revolution</returns>
    property NumberOfStepsPerRevolution: UInt16 read;
    /// <summary>Gets/Sets the current position of the motor</summary>
    /// <value>The current position (in steps)</value>
    /// <returns>The current position (in steps)</returns>
    property Position: Int32 read write;
    /// <summary>Gets the last known stopped position of the motor</summary>
    /// <returns>The last stopped position (in steps)</returns>
    property LastStoppedPosition: Int32 read;
    /// <summary>Enables/Disables the power to the motor</summary>
    /// <value>True enables the power</value>
    /// <returns>Read Only</returns>
    property Powered: Boolean write;
  end;

  /// <summary>This interface contains the extra commands for the motor stop channels</summary>
  IStoppableMotor = public interface(IMotor)
    /// <summary>MoveClockWise moves the motor clockwise with a certain acceleration and speed</summary>
    /// <param name="aAcceleration">The acceleration of the motor. (in motor steps / second²)</param>
    /// <param name="aSpeed">The speed of the motor. (in motor steps / second)</param>
    /// <param name="aStopChannels">one or more numbers of stopchannels in use for this movement</param>
    method MoveClockWiseUntilStopChannel(aAcceleration: Int32; aSpeed: UInt16; params aStopChannels: array of Byte);
    /// <summary>MoveCounterClockWise moves the motor counterclockwise with a certain acceleration and speed</summary>
    /// <param name="aAcceleration">The acceleration of the motor. (in motor steps / second²)</param>
    /// <param name="aSpeed">The speed of the motor. (in motor steps / second)</param>
    /// <param name="aStopChannels">one or more numbers of stopchannels in use for this movement</param>
    method MoveCounterClockWiseUntilStopChannel(aAcceleration: Int32; aSpeed: Word; params aStopChannels: array of Byte);  
    /// <summary>WaitUntilStopCondition stalls until a stopcondition happened.</summary>
    /// <returns>The channel that caused the Stopcondition (0 = Stalled)</returns>
    method WaitUntilStopCondition: Byte;
    /// <summary>Sets Stop Channel 1 with an instance of <see cref="MotorStopChannel"/></summary>
    /// <param name="aMotorStopChannel">An instance of <see cref="MotorStopChannel"/></param>
    method SetMotorStopChannel1(aMotorStopChannel: MotorStopChannel);
    /// <summary>Sets Stop Channel 2 with an instance of <see cref="MotorStopChannel"/></summary>
    /// <param name="aMotorStopChannel">An instance of <see cref="MotorStopChannel"/></param>
    method SetMotorStopChannel2(aMotorStopChannel: MotorStopChannel);
    /// <summary>MoveToPosition moves the motor to a specified position with a certain acceleration, speed and deceleration. It is a blocking operation and will return when it reached it's destination position.</summary>
    /// <param name="aPosition">The target position of the motor. (in motor steps)</param>
    /// <param name="aAcceleration">The acceleration of the motor. (in motor steps / second²)</param>
    /// <param name="aSpeed">The speed of the motor. (in motor steps / second)</param>
    /// <param name="aDeceleration">The deceleration of the motor. (in motor steps / second²)</param>
    /// <param name="aStopChannels">one or more numbers of stopchannels in use for this movement</param>
    /// <returns>The captured position</returns>
    method MoveToPositionWithCapture(aPosition: Int32; aAcceleration: Int32; aSpeed: Word; aDeceleration: Int32; params aStopChannels: array of Byte): Int32;
  end;

  /// <summary>Is the exception class raised by <see cref="IMotor"/> and <see cref="IStoppableMotor"/> implementations.</summary>
  MotorException = public class(Exception);

  StalledMotorException = public class(MotorException);
  StalledAndStoppedMotorException = public class(MotorException);

  /// <summary>Is the enumeration used by <see cref="IStoppableMotor"/> implementations.</summary>
  MotorStopChannelMode = public enum(
  /// <summary>When a stop situation is noticed the motor will stop ASAP.</summary>
  HardStop, 
  /// <summary>When a stop situation is noticed the motor decelerate and stop.</summary>
  Decelerate, 
  /// <summary>When a stop situation is noticed the motor will decelerate to a specified delta position and stop.</summary>
  MoveToDeltaPos,
  /// <summary>When a stop situation is noticed the motor will store the position for later use.</summary>
  StorePosition) of Byte;

  /// <summary>Is the Stop Channel class used by <see cref="IStoppableMotor"/> implementations.</summary>
  [aspect:&Equals]
  MotorStopChannel = public class
  private
  assembly
  public
    /// <summary>When True the motor will active the stopchannel when the input line is high.</summary>
    /// <value>True = High Level</value>
    /// <returns>True = High Level</returns>
    property StopAtHighLevel: Boolean;
    /// <summary>Select how to stopchannel should react when it is detected.</summary>
    /// <value>The stopchannel Mode</value>
    /// <returns>The stopchannel Mode</returns>
    property StopMode: MotorStopChannelMode;
    /// <summary>When in deleration mode this property set/gets the deceleration.</summary>
    /// <value>The deceleration. (in motor steps / second²)</value>
    /// <returns>The deceleration. (in motor steps / second²)</returns>
    property Deceleration: Int32;
    /// <summary>When in MoveToDeltaPos mode this property set/gets the delta movement distance.</summary>
    /// <value>The Delta move distance (in motor steps)</value>
    /// <returns>The Delta move distance (in motor steps)</returns>
    property MoveDelta: Int32;
  end;

implementation

end.