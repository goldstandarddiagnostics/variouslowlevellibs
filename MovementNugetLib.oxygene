﻿<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <ProductVersion>3.5</ProductVersion>
    <RootNamespace>System.Device.Motion</RootNamespace>
    <ProjectGuid>{ce870bf5-f319-478c-b303-eb8d4c1247a3}</ProjectGuid>
    <OutputType>Library</OutputType>
    <AssemblyName>System.Device.Motion</AssemblyName>
    <Configuration Condition="'$(Configuration)' == ''">Release</Configuration>
    <TargetFramework>.NETStandard2.0</TargetFramework>
    <Mode>Echoes</Mode>
    <Name>System.Device.Movement</Name>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Debug' ">
    <Optimize>false</Optimize>
    <OutputPath>.\bin\Debug</OutputPath>
    <DefineConstants>DEBUG;TRACE;</DefineConstants>
    <GeneratePDB>True</GeneratePDB>
    <GenerateMDB>True</GenerateMDB>
    <EnableAsserts>True</EnableAsserts>
    <CpuType>anycpu</CpuType>
    <XmlDoc>True</XmlDoc>
    <XmlDocWarningLevel>WarningOnPublicMembers</XmlDocWarningLevel>
    <EnableUnmanagedDebugging>False</EnableUnmanagedDebugging>
    <WarnOnCaseMismatch>True</WarnOnCaseMismatch>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Release' ">
    <Optimize>true</Optimize>
    <OutputPath>.\lib\netstandard2.0\</OutputPath>
    <GeneratePDB>True</GeneratePDB>
    <GenerateMDB>True</GenerateMDB>
    <EnableAsserts>False</EnableAsserts>
    <CpuType>anycpu</CpuType>
    <XmlDoc>True</XmlDoc>
    <XmlDocWarningLevel>WarningOnPublicMembers</XmlDocWarningLevel>
    <WarnOnCaseMismatch>True</WarnOnCaseMismatch>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include="Echoes">
      <Private>True</Private>
    </Reference>
  </ItemGroup>
  <ItemGroup>
    <Compile Include="H-Gantry.pas" />
    <Compile Include="H-GantryInterface.pas" />
    <Compile Include="IMotor.pas" />
    <Compile Include="LinearMovement.pas" />
    <Compile Include="LinearMovementInterface.pas" />
    <Compile Include="Properties\AssemblyInfo.pas" />
    <Compile Include="IPump.pas" />
    <EmbeddedResource Include="Properties\Resources.resx">
      <Generator>PublicResXFileCodeGenerator</Generator>
    </EmbeddedResource>
    <Compile Include="Properties\Resources.Designer.pas" />
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Properties\" />
  </ItemGroup>
  <ItemGroup>
    <ProjectReference Include="\\Mac\repos\GSD\Thundertbolt\thunderbolt-software-dev\Common\SubRepositories\oxygeneaspects\trunk\Source\Prism.StandardAspects\Prism.StandardAspects.oxygene">
      <Name>Prism.StandardAspects</Name>
      <Project>{fdfa9e51-2441-4785-9599-44a17d1b72c7}</Project>
      <Private>True</Private>
      <HintPath>\\Mac\repos\GSD\Thundertbolt\thunderbolt-software-dev\Common\SubRepositories\oxygeneaspects\trunk\Source\Prism.StandardAspects\bin\Debug\Prism.StandardAspects.dll</HintPath>
    </ProjectReference>
  </ItemGroup>
  <ItemGroup>
    <Content Include="License.txt">
      <SubType>Content</SubType>
    </Content>
    <Content Include="System.Device.Motion.nuspec">
      <SubType>Content</SubType>
    </Content>
  </ItemGroup>
  <Import Project="$(MSBuildExtensionsPath)\RemObjects Software\Elements\RemObjects.Elements.targets" />
</Project>