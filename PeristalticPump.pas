namespace Essy.PeristalticPump;

interface

uses
  Extreme.Mathematics.Curves.*,
  Extreme.Mathematics.LinearAlgebra,
  Extreme.Mathematics.Optimization,
  Essy.Motor,
  Essy.Pump;

type
  PeristalticPump = public class(IPump)
  private
    fResetPosition: Boolean := false;
    fMotor: IMotor;
    fVolumePerStep, fVolumePerRotation: Double;
    fLastAcc: UInt16;
    fCurve: PiecewiseLinearCurve := new PiecewiseLinearCurve(returnMonotoneArray(CURVEXVALUES, CURVEYVALUES));
    fPickedUpVolume: Double;
    method set_VolumePerRotation(value: Double);
    method returnMonotoneArray(aXArray, aYArray: Array of Double): Array of Point;
    method fVolumeToPosition(aVolume: Double): Int32;
    const
      CURVEXVALUES: array of Double = [0.0000, 	0.0062, 	0.0132, 	0.0240, 	0.0317, 	0.0418, 	0.0526, 	0.0627, 	0.0743, 	0.0844, 	0.0967, 	0.1076, 	
      0.1207, 	0.1316, 	0.1432, 	0.1563, 	0.1687, 	0.1834, 	0.1950, 	0.2082, 	0.2183, 	0.2276, 	0.2368, 	0.2376, 	0.2392, 	0.2461, 	0.2492, 	
      0.2477, 	0.2446, 	0.2415, 	0.2415, 	0.2446, 	0.2500, 	0.2562, 	0.2632, 	0.2740, 	0.2817, 	0.2918, 	0.3026, 	0.3127, 	0.3243, 	0.3344, 	
      0.3467, 	0.3576, 	0.3707, 	0.3816, 	0.3932, 	0.4063, 	0.4187, 	0.4334, 	0.4450, 	0.4582, 	0.4683, 	0.4776, 	0.4868, 	0.4876, 	0.4892, 	
      0.4961, 	0.4992, 	0.4977, 	0.4946, 	0.4915, 	0.4915, 	0.4946, 	0.5000, 	0.5062, 	0.5132, 	0.5240, 	0.5317, 	0.5418, 	0.5526, 	0.5627, 	
      0.5743, 	0.5844, 	0.5967, 	0.6076, 	0.6207, 	0.6316, 	0.6432, 	0.6563, 	0.6687, 	0.6834, 	0.6950, 	0.7082, 	0.7183, 	0.7276, 	0.7368, 	
      0.7376, 	0.7392, 	0.7461, 	0.7492, 	0.7477, 	0.7446, 	0.7415, 	0.7415, 	0.7446, 	0.7500, 	0.7562, 	0.7632, 	0.7740, 	0.7817, 	0.7918, 	
      0.8026, 	0.8127, 	0.8243, 	0.8344, 	0.8467, 	0.8576, 	0.8707, 	0.8816, 	0.8932, 	0.9063, 	0.9187, 	0.9334, 	0.9450, 	0.9582, 	0.9683, 	
      0.9776, 	0.9868, 	0.9876, 	0.9892, 	0.9961, 	0.9992, 	0.9977, 	0.9946, 	0.9915, 	0.9915, 	0.9946, 	1.0000];

      CURVEYVALUES: array of Double = [0, 	4, 	8, 	12, 	16, 	20, 	24, 	28, 	32, 	36, 	40, 	44, 	48, 	52, 	56, 	60, 	64, 	68, 	72, 	76, 	
      80, 	84, 	88, 	92, 	96, 	100, 	104, 	108, 	112, 	116, 	120, 	124, 	128, 	132, 	136, 	140, 	144, 	148, 	152, 	156, 	160, 	164, 	168, 	172, 	176, 	
      180, 	184, 	188, 	192, 	196, 	200, 	204, 	208, 	212, 	216, 	220, 	224, 	228, 	232, 	236, 	240, 	244, 	248, 	252, 	256, 	260, 	264, 	268, 	272, 	276, 	
      280, 	284, 	288, 	292, 	296, 	300, 	304, 	308, 	312, 	316, 	320, 	324, 	328, 	332, 	336, 	340, 	344, 	348, 	352, 	356, 	360, 	364, 	368, 	372, 	376, 	
      380, 	384, 	388, 	392, 	396, 	400, 	404, 	408, 	412, 	416, 	420, 	424, 	428, 	432, 	436, 	440, 	444, 	448, 	452, 	456, 	460, 	464, 	468, 	472, 	476, 	
      480, 	484, 	488, 	492, 	496, 	500, 	504, 	508, 	512]; 
  protected
  public
    constructor(aMotor: IMotor; aVolumePerRotation: Double);
    method StartAspirating(aVolume, aSpeed: Double);
    method StartAspirating(aSpeed: Double);
    method StopAspirating;
    method StartDispensing(aVolume, aSpeed: Double; aAlignRollers: Boolean);
    method StartDispensing(aVolume, aSpeed: Double);
    method StartDispensing(aSpeed: Double);
    method StopDispensing;
    method WaitForPumpActionToComplete;
    method ResetVolume;
    method CalculateTime(TotalVolume, RequestedVolume, Speed: Double): Double;
    property VolumePerRotation: Double write set_VolumePerRotation;
  end;
  
  PeristalticPumpException = public class(Exception);
  
implementation

uses 
  System.Collections.Generic, 
  System.Runtime.InteropServices;

constructor PeristalticPump(aMotor: IMotor; aVolumePerRotation: Double); 
begin
  fMotor := aMotor;
  fVolumePerRotation := aVolumePerRotation;
  fVolumePerStep := aVolumePerRotation / fMotor.NumberOfStepsPerRevolution;
  fPickedUpVolume := aVolumePerRotation * 10;
  fMotor.Position := fMotor.NumberOfStepsPerRevolution * 10;
end;

method PeristalticPump.StartAspirating(aVolume, aSpeed: Double); 
begin
  var motorSpeed := Convert.ToUInt16(aSpeed / fVolumePerStep);
  var accDec := motorSpeed * 3;
  fPickedUpVolume := fPickedUpVolume - aVolume;
  var pos := fVolumeToPosition(fPickedUpVolume);
  fMotor.StartMoveToPosition(pos, accDec, motorSpeed, accDec);
end;

method PeristalticPump.StartDispensing(aVolume, aSpeed: Double); 
begin
  self.StartDispensing(aVolume, aSpeed, false);
end;

method PeristalticPump.WaitForPumpActionToComplete; 
begin
  fMotor.WaitForMoveToPosition;
  if fResetPosition then
  begin
    fResetPosition := false;
    fPickedUpVolume := fMotor.Position * fVolumePerStep;
  end;
end;

method PeristalticPump.StartDispensing(aSpeed: Double);
begin
  var motorSpeed := Convert.ToUInt16(aSpeed / fVolumePerStep);
  var accDec := motorSpeed * 3;
  fLastAcc := accDec;
  fMotor.MoveClockWise(accDec, motorSpeed);
end;

method PeristalticPump.StopDispensing;
begin
  fMotor.Stop(fLastAcc);
  fPickedUpVolume := fMotor.Position * fVolumePerStep;
end;

method PeristalticPump.StartAspirating(aSpeed: Double);
begin
  var motorSpeed := Convert.ToUInt16(aSpeed / fVolumePerStep);
  var accDec := motorSpeed * 3;
  fLastAcc := accDec;
  fMotor.MoveCounterClockWise(accDec, motorSpeed);
end;

method PeristalticPump.StopAspirating;
begin
  fMotor.Stop(fLastAcc);
  fPickedUpVolume := fMotor.Position * fVolumePerStep;
end;

method PeristalticPump.set_VolumePerRotation(value: Double);
begin
  if value < 10 then raise new PeristalticPumpException('Volume per rotation should be bigger than 10, value was: ' + value.ToString());
  fVolumePerRotation := value;
  fVolumePerStep := value / fMotor.NumberOfStepsPerRevolution;
end;

method PeristalticPump.CalculateTime(TotalVolume, RequestedVolume, Speed: Double): Double;
begin
  var acc := Speed * 3;
  var dec := Speed * 9; 
  var lacc := (Speed * Speed) / (2 * acc);
  var ldec := (Speed * Speed) / (2 * dec);
  if TotalVolume <= (lacc + ldec) then
  begin  //triangle
    var lTop := (TotalVolume * dec) / (acc + dec);  
    if RequestedVolume < lTop then
    begin  //in acc part
      result := Math.Sqrt(RequestedVolume / acc);
    end
    else
    begin  //in dec part
      var tTop := Math.Sqrt(lTop / acc);
      result := tTop + Math.Sqrt((RequestedVolume - lTop) / dec);
    end;
  end
  else
  begin  //trapezoid
    var tacc := Speed / acc;
    var tdec := Speed / dec;
    var lSpeed := TotalVolume - (lacc + ldec);
    var tSpeed := lSpeed / Speed;
    var tTotal := tacc + tdec + tSpeed;
    if RequestedVolume <= lacc then
    begin  //in acc part
      result := Math.Sqrt(RequestedVolume / acc);
    end
    else if RequestedVolume >= (TotalVolume - ldec) then
    begin  //in dec part
      result := tTotal - Math.Sqrt((TotalVolume - RequestedVolume) / dec);
    end
    else
    begin  // in flat part
      result := tacc + (Speed / (RequestedVolume - lacc))
    end;
  end;
end;

method PeristalticPump.StartDispensing(aVolume, aSpeed: Double; aAlignRollers: Boolean);
begin
  var motorSpeed := Convert.ToUInt16(aSpeed / fVolumePerStep);
  var acc := motorSpeed * 3;
  var dec := motorSpeed * 9;
  var pos: Int32;
  if aAlignRollers then
  begin
    var volumePosition := Convert.ToInt32(Math.Round(aVolume / fVolumePerStep));
    pos := fMotor.Position + volumePosition;
    var stepsPerQuarterRevolution := fMotor.NumberOfStepsPerRevolution div 4;
    var mods := pos mod stepsPerQuarterRevolution;
    if mods > 0 then
    begin
      var divs := pos div stepsPerQuarterRevolution;
      pos := stepsPerQuarterRevolution * (divs + 1);  
    end; 
    fResetPosition := true;
  end
  else
  begin
    fPickedUpVolume := fPickedUpVolume + aVolume;
    pos := fVolumeToPosition(fPickedUpVolume);
  end;
  fMotor.StartMoveToPosition(pos, acc, motorSpeed, dec);
end;

method PeristalticPump.fVolumeToPosition(aVolume: Double): Int32;
begin
  var numberOfRevs := aVolume / fVolumePerRotation;
  var numberOfFullRevs := Math.Truncate(numberOfRevs);
  var subRevPosPercentage := numberOfRevs - numberOfFullRevs;
  var subRevPos := fCurve.ValueAt(subRevPosPercentage);
  result := Convert.ToInt32(Math.Round(subRevPos + (numberOfFullRevs * fMotor.NumberOfStepsPerRevolution)));
end;

method PeristalticPump.returnMonotoneArray(aXArray, aYArray: Array of Double): Array of Point;
begin
  var tempList := new List<Point>;
  var lastVal := Double.MinValue;
  for each number in aXArray index indx do
  begin
    if number > lastVal then  //curve must be ascending
    begin
      lastVal := number;
      var tempPoint := new Point(number, aYArray[indx]);
      tempList.Add(tempPoint);
    end;
  end;
  result := tempList.ToArray();
end;

method PeristalticPump.ResetVolume;
begin
  fMotor.Position := fMotor.NumberOfStepsPerRevolution * 10;
  fPickedUpVolume := fMotor.Position * fVolumePerStep;
end;

end.