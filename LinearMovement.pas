﻿namespace System.Device.Motion.LinearMovements;

interface

uses 
  System.Threading,
  System.Device.Motion;

type
  /// <summary>This class represents a LinearMovement attached to motor.</summary>
  LinearMovement = public class(ILinearMovement)
  private
    var fMMPerStep: Double;
    method set_Powered(value: Boolean);
    method get_LastStoppedPosition: Double;
    method set_Position(value: Double);
    method get_Position: Double;
    method fRoundToInt32(aValue: Double): Int32;
    method fToMotorSteps(aDistanceInMM: Double): Int32;
    method fToMM(aMotorSteps: Int32): Double;
  protected
  public
    property UsedMotor: IStoppableMotor; 
    property DistancePerRotation: Double; 
    /// <summary>MoveToPosition moves the LinearMovement to a specified position with a certain acceleration, speed and deceleration. It is a blocking operation and will return when it reached it's destination position.</summary>
    /// <param name="aPosition">The target position of the LinearMovement. (in mm)</param>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    /// <param name="aDeceleration">The deceleration of the LinearMovement. (in mm / second²)</param>
    method MoveToPosition(aPosition: Double; aAcceleration: Double; aSpeed: Double; aDeceleration: Double);
    /// <summary>MoveToPosition moves the LinearMovement to a specified position with a certain acceleration, speed and deceleration. It is a non blocking operation and will return immediatly (use this in combination with WaitForMoveToPosition).</summary>
    /// <param name="aPosition">The target position of the LinearMovement. (in mm)</param>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    /// <param name="aDeceleration">The deceleration of the LinearMovement. (in mm / second²)</param>
    method StartMoveToPosition(aPosition: Double; aAcceleration: Double; aSpeed: Double; aDeceleration: Double);
    /// <summary>WaitForMoveToPosition waits until StartMoveToPosition finishes.</summary>
    method WaitForMoveToPosition();
    /// <summary>MoveClockWise moves the LinearMovement clockwise with a certain acceleration and speed</summary>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    method MoveClockWise(aAcceleration: Double; aSpeed: Double);
    /// <summary>MoveCounterClockWise moves the LinearMovement counterclockwise with a certain acceleration and speed</summary>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    method MoveCounterClockWise(aAcceleration: Double; aSpeed: Double);
    /// <summary>Stops the LinearMovement with a certain deceleration</summary>
    /// <param name="aDeceleration">The deceleration of the LinearMovement. (in mm / second²)</param>
    method Stop(aDeceleration: Double);
    /// <summary>Gets/Sets the current position of the LinearMovement</summary>
    /// <value>The current position (in mm)</value>
    /// <returns>The current position (in mm)</returns>
    property Position: Double read get_Position write set_Position;
    /// <summary>Enables/Disables the power to the LinearMovement</summary>
    /// <value>True enables the power</value>
    /// <returns>True if power is enabled</returns>
    property Powered: Boolean write set_Powered;
    /// <summary>When enabled no negative coordinates are used</summary>
    property NoNegativeCoordinates: Boolean := false;
     /// <summary>Gets the last stopped position of the LinearMovement</summary>
    /// <returns>The last stopped position (in mm)</returns>
    property LastStoppedPosition: Double read get_LastStoppedPosition;
    /// <summary>
    /// This tells the linear movent to use inverse coordinates
    /// </summary>
    /// <value>true = inversion active</value>
    property InvertCoordinates: Boolean;
    /// <summary>MoveClockWiseUntilStopChannel moves the LinearMovement clockwise with a certain acceleration and speed until a stopchannel happens</summary>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    /// <param name="aStopChannels">one or more numbers of stopchannels in use for this movement</param>
    method MoveClockWiseUntilStopChannel(aAcceleration: Double; aSpeed: Double; params aStopChannels: array of Byte);
    /// <summary>MoveCounterClockWise moves the LinearMovement counterclockwise with a certain acceleration and speed until a stopchannel happens</summary>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    /// <param name="aStopChannels">one or more numbers of stopchannels in use for this movement</param>
    method MoveCounterClockWiseUntilStopChannel(aAcceleration: Double; aSpeed: Double; params aStopChannels: array of Byte);  
    /// <summary>WaitUntilStopCondition stalls until a stopcondition happened.</summary>
    /// <returns>The channel that caused the Stopcondition (0 = Stalled)</returns>
    method WaitUntilStopCondition(aTimeout: TimeSpan): Byte; 
    method SetStopChannel1(aLinearStopChannel: LinearStopChannel);
    method SetStopChannel2(aLinearStopChannel: LinearStopChannel);
    method MoveToPositionWithCapture(aPosition: Double; aAcceleration: Double; aSpeed: Double; aDeceleration: Double; params aStopChannels: array of Byte): Double;
    /// <summary>Creates a new LinearMovement object.</summary>
    /// <param name="aUsedMotor">Any motor that implements the IStoppableMotor interface</param>
    /// <param name="aDistancePerRotation">The distance the LinearMovement travels in one revolution of the motor. (in mm)</param>
    constructor(aUsedMotor: IStoppableMotor; aDistancePerRotation: Double);
  end;

implementation

constructor LinearMovement(aUsedMotor: IStoppableMotor; aDistancePerRotation: Double);
begin
  self.UsedMotor := aUsedMotor;
  self.DistancePerRotation := aDistancePerRotation;
  self.fMMPerStep := aDistancePerRotation / aUsedMotor.NumberOfStepsPerRevolution;
end;

method LinearMovement.MoveToPosition(aPosition: Double; aAcceleration: Double; aSpeed: Double; aDeceleration: Double);
begin
  var pos := fToMotorSteps(aPosition);
  if self.NoNegativeCoordinates and (pos < 0) then pos := 0;
  if InvertCoordinates then pos := 0 - pos;
  self.UsedMotor.MoveToPosition(pos, fToMotorSteps(aAcceleration), fToMotorSteps(aSpeed), fToMotorSteps(aDeceleration));
end;

method LinearMovement.fRoundToInt32(aValue: Double): Int32;
begin
  result := Convert.ToInt32(Math.Round(aValue));
end;

method LinearMovement.fToMotorSteps(aDistanceInMM: Double): Int32;
begin
  result := fRoundToInt32(aDistanceInMM / self.fMMPerStep);
end;

method LinearMovement.StartMoveToPosition(aPosition: Double; aAcceleration: Double; aSpeed: Double; aDeceleration: Double);
begin
  var pos := fToMotorSteps(aPosition);  
  if self.NoNegativeCoordinates and (pos < 0) then pos := 0;
  if InvertCoordinates then pos := 0 - pos;
  self.UsedMotor.StartMoveToPosition(pos, fToMotorSteps(aAcceleration), fToMotorSteps(aSpeed), fToMotorSteps(aDeceleration));
end;

method LinearMovement.WaitForMoveToPosition();
begin
  self.UsedMotor.WaitForMoveToPosition();
  //System.Threading.Thread.Sleep(2000);
  //Console.WriteLine('R' + UsedMotor.Position.ToString());
end;

method LinearMovement.MoveClockWise(aAcceleration: Double; aSpeed: Double);
begin
  if InvertCoordinates then 
    self.UsedMotor.MoveCounterClockWise(fToMotorSteps(aAcceleration), fToMotorSteps(aSpeed))
  else
    self.UsedMotor.MoveClockWise(fToMotorSteps(aAcceleration), fToMotorSteps(aSpeed))
end;

method LinearMovement.MoveCounterClockWise(aAcceleration: Double; aSpeed: Double);
begin
  if InvertCoordinates then 
    self.UsedMotor.MoveClockWise(fToMotorSteps(aAcceleration), fToMotorSteps(aSpeed))
  else
    self.UsedMotor.MoveCounterClockWise(fToMotorSteps(aAcceleration), fToMotorSteps(aSpeed))
end;

method LinearMovement.Stop(aDeceleration: Double);
begin
  self.UsedMotor.Stop(fToMotorSteps(aDeceleration));
end;

method LinearMovement.get_Position: Double;
begin
  result := fToMM(self.UsedMotor.Position);
  if InvertCoordinates then result := 0 - result;
end;

method LinearMovement.set_Position(value: Double);
begin
  var pos := fToMotorSteps(value);
  if InvertCoordinates then pos := 0 - pos;
  self.UsedMotor.Position := pos;
end;

method LinearMovement.set_Powered(value: Boolean);
begin
  self.UsedMotor.Powered := value;
end;

method LinearMovement.fToMM(aMotorSteps: Int32): Double;
begin
  result :=  self.fMMPerStep * aMotorSteps;
end;

method LinearMovement.MoveClockWiseUntilStopChannel(aAcceleration: Double; aSpeed: Double;params aStopChannels: array of Byte);
begin
  self.UsedMotor.MoveClockWiseUntilStopChannel(fToMotorSteps(aAcceleration), fToMotorSteps(aSpeed), aStopChannels);
end;

method LinearMovement.MoveCounterClockWiseUntilStopChannel(aAcceleration: Double; aSpeed: Double; params aStopChannels: array of Byte);
begin
   self.UsedMotor.MoveCounterClockWiseUntilStopChannel(fToMotorSteps(aAcceleration), fToMotorSteps(aSpeed), aStopChannels);
end;

method LinearMovement.SetStopChannel1(aLinearStopChannel: LinearStopChannel);
begin
  var tempMotorStopChannel := new MotorStopChannel;
  tempMotorStopChannel.Deceleration := fToMotorSteps(aLinearStopChannel.Deceleration);
  tempMotorStopChannel.MoveDelta := fToMotorSteps(aLinearStopChannel.MoveDelta);
  tempMotorStopChannel.StopAtHighLevel := aLinearStopChannel.StopAtHighLevel;
  tempMotorStopChannel.StopMode := aLinearStopChannel.StopMode;
  self.UsedMotor.SetMotorStopChannel1(tempMotorStopChannel);
end;

method LinearMovement.SetStopChannel2(aLinearStopChannel: LinearStopChannel);
begin
  var tempMotorStopChannel := new MotorStopChannel;
  tempMotorStopChannel.Deceleration := fToMotorSteps(aLinearStopChannel.Deceleration);
  tempMotorStopChannel.MoveDelta := fToMotorSteps(aLinearStopChannel.MoveDelta);
  tempMotorStopChannel.StopAtHighLevel := aLinearStopChannel.StopAtHighLevel;
  tempMotorStopChannel.StopMode := aLinearStopChannel.StopMode;
  self.UsedMotor.SetMotorStopChannel2(tempMotorStopChannel);
end;

method LinearMovement.WaitUntilStopCondition(aTimeout: TimeSpan): Byte;
begin
  var t := new System.Threading.Tasks.Task<Byte>(method begin exit self.UsedMotor.WaitUntilStopCondition(); end);
  t.Start();
  if t.Wait(aTimeout) then
  begin
    exit t.Result;
  end
  else 
  begin
    //self.Stop(1000);
    exit 255; //indicates a timeout
  end;
end;

method LinearMovement.MoveToPositionWithCapture(aPosition: Double; aAcceleration: Double; aSpeed: Double; aDeceleration: Double; params aStopChannels: array of Byte): Double;
begin
  var pos := fToMotorSteps(aPosition);
  if self.NoNegativeCoordinates and (pos < 0) then pos := 0; 
  result := fToMM(self.UsedMotor.MoveToPositionWithCapture(pos, fToMotorSteps(aAcceleration), fToMotorSteps(aSpeed), fToMotorSteps(aDeceleration), aStopChannels));  
end;

method LinearMovement.get_LastStoppedPosition: Double;
begin
  result := fToMM(self.UsedMotor.LastStoppedPosition);
  if InvertCoordinates then result := 0 - result;
end;

end.