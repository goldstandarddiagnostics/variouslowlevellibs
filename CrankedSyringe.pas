﻿///<summary>This namespace contains all cranked syringe related types.</summary>
namespace Essy.CrankedSyringe;

interface

uses 
  Essy.Motor,
  Essy.Pump;

type
  ///<summary>A syringe that uses a crank to move the piston.</summary>
  CrankedSyringe = public class(ISyringe)
  private
    fMotor: IMotor;
    fPickedUpVolume: Double := 0;
    fCrankLength: Double;
    fPistonPinToCrankCentreLength: Double; 
    fuLPerMM: Double; 
    fMaxPickupVolume: Double;
    fHalfOfPulsesPerRotation: Double;
    method fConvertVolumeToMotorSteps(aVolume: Double): Int32;
  protected
  public
    method WaitForPumpActionToComplete;
    method StartDispensing(aVolume: System.Double; aSpeed: System.Double);
    method StartAspirating(aVolume: System.Double; aSpeed: System.Double);
    method StartDispenseAll(aSpeed: System.Double);
    constructor(aMotor: IMotor; aCrankLength ,aPistonPinToCrankCentreLength, auLPerMM, aMaxPickupVolume: Double);
    property MaxPossibleVolume: System.Double read fMaxPickupVolume;
    property AspiratedVolume: System.Double read fPickedUpVolume;
  end;
  
implementation

/// <summary>StartAspirating aspirates a certain volume with a selected speed . It is a non blocking operation and must be used in combination with WaitForPumpActionToComplete.</summary>
/// <param name="aVolume">the volume to aspirate. (in µL)</param>
/// <param name="aSpeed">The speed of the pump. (in  µL / second)</param>
method CrankedSyringe.StartAspirating(aVolume: System.Double; aSpeed: System.Double);
begin
  self.fPickedUpVolume := self.fPickedUpVolume + aVolume;
  var steps := self.fConvertVolumeToMotorSteps(self.fPickedUpVolume);
  var speed := iif(aVolume > 0, aSpeed / aVolume, 100.0);
  var speedSteps := Convert.ToInt32(steps * speed); 
  self.fMotor.StartMoveToPosition(steps, speedSteps / 2, speedSteps, speedSteps * 3);
end;

/// <summary>StartDispensing dispenses a certain volume with a selected speed . It is a non blocking operation and must be used in combination with WaitForPumpActionToComplete.</summary>
/// <param name="aVolume">the volume to dispense. (in µL)</param>
/// <param name="aSpeed">The speed of the pump. (in  µL / second)</param>
method CrankedSyringe.StartDispensing(aVolume: System.Double; aSpeed: System.Double);
begin
  self.fPickedUpVolume := self.fPickedUpVolume - aVolume;
  var steps := self.fConvertVolumeToMotorSteps(self.fPickedUpVolume);
  var distance := self.fMotor.Position - steps;
  var speed := iif(aVolume > 0, aSpeed / aVolume, 100.0);
  var speedSteps := Convert.ToInt32(distance * speed); 
  self.fMotor.StartMoveToPosition(steps, speedSteps / 2, speedSteps, speedSteps * 3);
end;

/// <summary>WaitForPumpActionToComplete waits until the pump action is complete.</summary>
method CrankedSyringe.WaitForPumpActionToComplete;
begin
  self.fMotor.WaitForMoveToPosition();
end;

/// <summary>The constructor of a Cranked Syringe</summary>
/// <param name="aMotor">any motor that implements the <see cref="IStoppableMotor"/> interface.</param>
/// <param name="aCrankLength">The length of the crank. (in mm)</param>
/// <param name="aPistonPinToCrankCentreLength">The length between the cylinder hinge point and the centre of the crank. (in mm)</param>
/// <param name="auLPerMM">How many µL per moved mm (in µL/mm)</param>
/// <param name="aMaxPickupVolume">Maximum allowed pickup volume. (in µL)</param>
constructor CrankedSyringe(aMotor: IMotor; aCrankLength ,aPistonPinToCrankCentreLength, auLPerMM, aMaxPickupVolume: Double);
begin
  self.fMotor := aMotor;
  self.fCrankLength := aCrankLength;
  self.fPistonPinToCrankCentreLength := aPistonPinToCrankCentreLength;
  self.fuLPerMM := auLPerMM;
  self.fMaxPickupVolume := aMaxPickupVolume;
  self.fHalfOfPulsesPerRotation := fMotor.NumberOfStepsPerRevolution / 2.0;
  self.fMotor.Position := 0;
end;

method CrankedSyringe.fConvertVolumeToMotorSteps(aVolume: Double): Int32;
begin
  var L0 := fPistonPinToCrankCentreLength;
  var L1 := fCrankLength;
  var displacement := aVolume / self.fuLPerMM;
  var L := displacement + L0 - L1;
  var L3 := ((L0 * L0) + (L1 * L1) - (L * L)) / (2 * L0 * L1);
  var alpha := Math.Acos(L3);
  result := Convert.ToInt32(Math.Round(alpha * (fHalfOfPulsesPerRotation / Math.PI)));
end;

/// <summary>Dispenses all the aspirated fluid. This is non blocking operation and should be used in combination with WaitForPumpActionToComplete</summary>
/// <param name="aSpeed">The speed of the pump. (in  µL / second)</param>
method CrankedSyringe.StartDispenseAll(aSpeed: System.Double);
begin
  var steps: Int32;
  var speed: Double;
  var speedSteps: Int32;
  if self.fPickedUpVolume > 0 then 
  begin
    steps := self.fConvertVolumeToMotorSteps(self.fPickedUpVolume);
    speed := aSpeed / self.fPickedUpVolume;
    speedSteps := Convert.ToInt32(steps * speed);
  end
  else
  begin
    speedSteps := 100;
  end;
  self.fMotor.StartMoveToPosition(0, speedSteps / 2, speedSteps, speedSteps * 3);
  self.fPickedUpVolume := 0;
end;

end.