﻿namespace System.Device.Motion.LinearMovements;

interface

type

  IHGantry = public interface
    method MoveToPosition(aX, aY: Double);
    method MoveToPosition(aX, aY, aAccelleration, aSpeed, aDecelleration: Double);
    method SetPosition(aX, aY: Double);
    property XPos: Double read;
    property YPos: Double read;
  end;
  
implementation

end.
