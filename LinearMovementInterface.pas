﻿namespace System.Device.Motion.LinearMovements;

interface

uses 
  System.Device.Motion;

type

  ILinearMovement = public interface
    /// <summary>MoveToPosition moves to a specified position with a certain acceleration, speed and deceleration. It is a blocking operation and will return when it reached it's destination position.</summary>
    /// <param name="aPosition">The target position. (in mm)</param>
    /// <param name="aAcceleration">The acceleration. (in mm / second²)</param>
    /// <param name="aSpeed">The speed. (in mm / second)</param>
    /// <param name="aDeceleration">The deceleration. (in mm / second²)</param>
    method MoveToPosition(aPosition: Double; aAcceleration: Double; aSpeed: Double; aDeceleration: Double);
    /// <summary>MoveToPosition moves to a specified position with a certain acceleration, speed and deceleration. It is a non blocking operation and will return immediatly (use this in combination with WaitForMoveToPosition).</summary>
    /// <param name="aPosition">The target position. (in mm)</param>
    /// <param name="aAcceleration">The acceleration. (in mm / second²)</param>
    /// <param name="aSpeed">The speed. (in mm / second)</param>
    /// <param name="aDeceleration">The deceleration. (in mm / second²)</param>
    method StartMoveToPosition(aPosition: Double; aAcceleration: Double; aSpeed: Double; aDeceleration: Double);
    /// <summary>WaitForMoveToPosition waits until StartMoveToPosition finishes.</summary>
    method WaitForMoveToPosition();
    /// <summary>MoveClockWise moves the LinearMovement clockwise with a certain acceleration and speed</summary>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    method MoveClockWise(aAcceleration: Double; aSpeed: Double);
    /// <summary>MoveCounterClockWise moves the LinearMovement counterclockwise with a certain acceleration and speed</summary>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    method MoveCounterClockWise(aAcceleration: Double; aSpeed: Double);
    /// <summary>Stops the LinearMovement with a certain deceleration</summary>
    /// <param name="aDeceleration">The deceleration of the LinearMovement. (in mm / second²)</param>
    method Stop(aDeceleration: Double);
    /// <summary>MoveClockWiseUntilStopChannel moves the LinearMovement clockwise with a certain acceleration and speed until a stopchannel happens</summary>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    /// <param name="aStopChannels">one or more numbers of stopchannels in use for this movement</param>
    method MoveClockWiseUntilStopChannel(aAcceleration: Double; aSpeed: Double; params aStopChannels: array of Byte);
    /// <summary>MoveCounterClockWise moves the LinearMovement counterclockwise with a certain acceleration and speed until a stopchannel happens</summary>
    /// <param name="aAcceleration">The acceleration of the LinearMovement. (in mm / second²)</param>
    /// <param name="aSpeed">The speed of the LinearMovement. (in mm / second)</param>
    /// <param name="aStopChannels">one or more numbers of stopchannels in use for this movement</param>
    method MoveCounterClockWiseUntilStopChannel(aAcceleration: Double; aSpeed: Double; params aStopChannels: array of Byte);  
    /// <summary>WaitUntilStopCondition stalls until a stopcondition happened.</summary>
    /// <returns>The channel that caused the Stopcondition (0 = Stalled) (255 = Timed Out)</returns>
    method WaitUntilStopCondition(aTimeout: TimeSpan): Byte; 
    method SetStopChannel1(aStopChannel: LinearStopChannel);
    method SetStopChannel2(aStopChannel: LinearStopChannel);
    method MoveToPositionWithCapture(aPosition: Double; aAcceleration: Double; aSpeed: Double; aDeceleration: Double; params aStopChannels: array of Byte): Double;
    property DistancePerRotation: Double read write; 
    /// <summary>Gets/Sets the current position of the LinearMovement</summary>
    /// <value>The current position (in mm)</value>
    /// <returns>The current position (in mm)</returns>
    property Position: Double read write;
    /// <summary>Gets the last stopped position of the LinearMovement</summary>
    /// <returns>The last stopped position (in mm)</returns>
    property LastStoppedPosition: Double read;
    /// <summary>Enables/Disables the power to the LinearMovement</summary>
    /// <value>True enables the power</value>
    /// <returns>True if power is enabled</returns>
    property Powered: Boolean write;
    /// <summary>When enabled no negative coordinates are used</summary>
    property NoNegativeCoordinates: Boolean read write;
    /// <summary>
    /// This tells the linear movent to use inverse coordinates
    /// </summary>
    /// <value>true = inversion active</value>
    property InvertCoordinates: Boolean read write;
  end;

  LinearStopChannel = public class
  public
    property StopAtHighLevel: Boolean;
    property StopMode: MotorStopChannelMode;
    /// <summary>Gets/Sets the deceleration during a stop channel.</summary>
    /// <value>The deceleration during a stop channel. (in mm / second²)</value>
    /// <returns>The deceleration during a stop channel. (in mm / second²)</returns>
    property Deceleration: Double;
    property MoveDelta: Double;
  end;
  
implementation

end.
