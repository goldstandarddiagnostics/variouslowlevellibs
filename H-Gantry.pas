﻿namespace System.Device.Motion.LinearMovements;

interface

uses
  System.Collections.Generic,
  System.Device.Motion,
  System.Device.Motion.LinearMovements;

type

  HGantry = public class(IHGantry) 
  private
    method get_XPos: Double;
    method get_YPos: Double;
    fMotorA, fMotorB: IMotor;
    fStepsPerMM: Double;
    fPrevAPos, fPrevBPos: Integer;
    fPrevXPos, fPrevYPos: Double;
    const
      MAXSPEED = 500;
      SMOOTHSPEED = 150;
  protected
  public
    method MoveToPosition(aX, aY: Double);
    method MoveToPosition(aX, aY, aAccelleration, aSpeed, aDecelleration: Double);
    method SetPosition(aX, aY: Double);
    property XPos: Double read get_XPos;
    property YPos: Double read get_YPos;
    constructor(aMotorA, aMotorB: IMotor; aDistancePerRotation: Double);
  end;

implementation

constructor HGantry(aMotorA: IMotor; aMotorB: IMotor; aDistancePerRotation: Double);
begin
  self.fMotorA := aMotorA;
  self.fMotorB := aMotorB;
  self.fStepsPerMM := aMotorA.NumberOfStepsPerRevolution / aDistancePerRotation;
end;

method HGantry.MoveToPosition(aX: Double; aY: Double);
begin
  var s := iif((Math.Abs(fPrevXPos - aX) > 11) or (Math.Abs(fPrevYPos - aY) > 11), MAXSPEED, SMOOTHSPEED);
  MoveToPosition(aX, aY, s * 5, s, s * 2)
end;

method HGantry.get_XPos: Double;
begin
  var a := fMotorA.Position;
  var b := fMotorB.Position;
  result := 0 - ((a + b) / 2.0) / fStepsPerMM; 
end;

method HGantry.get_YPos: Double;
begin
  var a := fMotorA.Position;
  var b := fMotorB.Position;
  result := ((a - b) / 2.0) / fStepsPerMM; 
end;

method HGantry.SetPosition(aX: Double; aY: Double);
begin
  var a := Math.Round(((0 - aX) + aY) * fStepsPerMM);
  var b := Math.Round(((0 - aX) - aY) * fStepsPerMM);
  fMotorA.Position := Int32(a);
  fMotorB.Position := Int32(b);
  fPrevAPos := Int32(a);
  fPrevBPos := Int32(b);
  fPrevXPos := aX;
  fPrevYPos := aY;
end;

method HGantry.MoveToPosition(aX: Double; aY: Double; aAccelleration: Double; aSpeed: Double; aDecelleration: Double);
begin
  var s := aSpeed * fStepsPerMM;
  var acc := aAccelleration * fStepsPerMM;
  var dec := aDecelleration * fStepsPerMM;
  var a := Math.Round(((0 - aX) + aY) * fStepsPerMM);
  var b := Math.Round(((0 - aX) - aY) * fStepsPerMM);
  var deltaA := Math.Abs(a - fPrevAPos);
  var deltaB := Math.Abs(b - fPrevBPos);
  var speedA := 0;
  var accA := 0;
  var decA := 0;
  var speedB := 0;
  var accB := 0;
  var decB := 0;
  if deltaA > deltaB then
  begin
    var factor := deltaB / deltaA;
    speedA := Int32(s);
    accA := Int32(acc);
    decA := Int32(dec);
    speedB := Int32(s * factor);
    accB := Int32(acc * factor);
    decB := Int32(dec * factor);
  end
  else
  begin
    var factor := deltaA / deltaB;
    speedB := Int32(s);
    accB := Int32(acc);
    decB := Int32(dec);
    speedA := Int32(s * factor);
    accA := Int32(acc * factor);
    decA := Int32(dec * factor);
  end;
  if accA < 0 then accA := 0;
  if accB < 0 then accB := 0;
  if speedA < 0 then speedA := 0;
  if speedB < 0 then speedB := 0;
  if decA < 0 then decA := 0;
  if decB < 0 then decB := 0;
  fMotorA.StartMoveToPosition(Int32(a), accA, speedA, decA);
  fMotorB.StartMoveToPosition(Int32(b), accB, speedB, decB);
  fMotorA.WaitForMoveToPosition();
  fMotorB.WaitForMoveToPosition();
  fPrevAPos := Int32(a);
  fPrevBPos := Int32(b);
  fPrevXPos := aX;
  fPrevYPos := aY;
end;

end.
